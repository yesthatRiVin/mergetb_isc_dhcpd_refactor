mergetb_isc_dhcpd
=================

This role installs and configures the dhcpd service on specific hosts. It also creates a dhcpd.conf file and populates it with subnet information and host information that is pulled from a YAML dictionary.

Requirements
------------

>[!WARNING]
> This role depends on having a YAML dictionary to store subnet and host information. Use the template in [role variables](#role-variables) for reference.

Role Variables
--------------

This is an example template of what the YAML dictionary should be:
```yaml
network_subnets:
  - subnet: 192.168.122.0
    netmask: 255.255.255.0
    range_start: 192.168.122.2
    range_end: 192.168.122.254

host_mappings:
  vm0:
    ip: 192.168.122.2
    mac: 52:54:00:6c:00:6a
  vm1:
    ip: 192.168.122.3
    mac: 52:54:00:6c:00:6b
```

Dependencies
------------

No known dependecies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- name: dhcp server installation
  hosts: dhcp_server
  gather_facts: true
  become: true
  roles:
    - role: mergetb_isc_dhcpd
```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
